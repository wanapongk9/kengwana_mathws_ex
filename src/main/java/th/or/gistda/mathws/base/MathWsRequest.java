package th.or.gistda.mathws.base;

import java.util.List;

public class MathWsRequest extends Object implements IMathWsRequest {

	
	private String servName;
	private String servOp;
	private List<Double> inputA;
	private List<Double> inputB;
	private List<Double> outputC;
	
	@Override
	public void setName(String name) {
		servName = name;
	}
	
	@Override
	public String getName() {
		return servName;
	}

	@Override
	public void setOperation(String op) {
		servOp = op;
	}

	@Override
	public String getOperation() {
		return servOp;
	}

	@Override
	public void setInputA(List<Double> a) {
		inputA = a;
	}

	@Override
	public List<Double> getInputA() {
		return inputA;
	}

	@Override
	public void setInputB(List<Double> b) {
		inputB = b;
	}

	@Override
	public List<Double> getInputB() {
		return inputB;
	}

	@Override
	public void setOutputC(List<Double> c) {
		outputC = c;
	}

	@Override
	public List<Double> getOutputC() {
		return outputC;
	}

	@Override
	public boolean equals(Object obj) {
		
		IMathWsRequest compare = (IMathWsRequest) obj;
		
		if (servName != null) {
			if (!servName.equals(compare.getName())) {return false;}
		}
		
		if (servOp != null) {
			if (!servOp.equals(compare.getOperation())) {return false;}
		}
		
		if (inputA != null) {
			if (!inputA.equals(compare.getInputA())) {return false;}
		}
		
		if (inputB != null) {
			if (!inputB.equals(compare.getInputB())) {return false;}
		}
		
		if (outputC != null) {
			if (!outputC.equals(compare.getOutputC())) {return false;}
		}
		
		return true;
	}
}
