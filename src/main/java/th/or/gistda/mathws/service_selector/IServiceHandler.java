package th.or.gistda.mathws.service_selector;

import th.or.gistda.mathws.base.IMathWsRequest;
import th.or.gistda.mathws.base.IMathWsResponse;

public interface IServiceHandler {
	public void handle(IMathWsRequest req, IMathWsResponse res);
}
