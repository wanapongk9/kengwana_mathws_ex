package th.or.gistda.mathws.service;

import java.util.List;

public class ServiceHandler_Test {
	
	protected static final Double tolerancePercentage = 1e-12;
	
	protected boolean verifyErrorTolerance(List<Double> expected, List<Double> actual) {
		
		if (expected.size() != actual.size())
			return false;
		
		for (int idx=0;idx<expected.size();idx++) {
			Double eVal = expected.get(idx);
			Double aVal = actual.get(idx);
			if (((Math.abs(eVal - aVal)) / Math.abs(eVal)) >= tolerancePercentage)
				return false;
		}
		
		return true;
	}
}
